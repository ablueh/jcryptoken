package jcryptoken;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * TODO add more tests
 */
public class TestAll {

	private static final byte[] defaultKey = "abcdefghijklmnop".getBytes();
	
	@Test
	public void shouldShowAValidToken() {
		Token token = 
				new Token(defaultKey)
					.setId("1")
					.expiresIn(10, Duration.DAYS)
				    .set("role", "admin")
				    .set("active", "yes");
		
		assertFalse(token.isExpired());
		
		assertTrue(token.isValid());
		
		assertFalse(token.toString().isEmpty());
		
		System.out.println(token.toString());
		
	}

	@Test
	public void shouldEncryptAndDecryptAToken() {
		String encrypted = new Token(defaultKey)
			.setId("1")
			.expiresIn(10, Duration.DAYS)
			.set("role", "admin")
			.set("active", "yes")
			.toString();
		
		Token token = new Token(defaultKey, encrypted);
		
		assertFalse(token.isExpired());
		
		assertTrue(token.isValid());
		
		assertEquals("admin", token.get("role"));
		assertEquals("yes", token.get("active"));
	}

}
