package jcryptoken;

import java.util.Calendar;
import java.util.Date;

public enum Duration {
	SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS;
	
	public Date fromNow(int unit) {
		Calendar c = Calendar.getInstance();
		switch(this) {
		case DAYS:
			c.add(Calendar.DAY_OF_MONTH, unit);
			break;
		case HOURS:
			c.add(Calendar.HOUR_OF_DAY, unit);
			break;
		case MINUTES:
			c.add(Calendar.MINUTE, unit);
			break;
		case MONTHS:
			c.add(Calendar.MONTH, unit);
			break;
		case SECONDS:
			c.add(Calendar.SECOND, unit);
			break;
		case WEEKS:
			c.add(Calendar.WEEK_OF_YEAR, unit);
			break;
		case YEARS:
			c.add(Calendar.YEAR, unit);
			break;
		}
		return c.getTime();
	}
}
